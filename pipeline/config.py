# pipeline parameters
DESCRIPTION='Pytorch fashion mnist kubeflow ci/cd/ct pipeline demo'
PROJECT_NAME = 'pytorchjob-katib'
PIPELINE_NAME = PROJECT_NAME + '-pipeline'
EXPERIMENT_VERSION = 'v07'
EXPERIMENT_NAME = PIPELINE_NAME + '-' + EXPERIMENT_VERSION
EXPERIMENT_NAMESPACE = 'admin'
NAME = EXPERIMENT_NAME
DATASET_PATH = '/workspace/project/' + PROJECT_NAME + '/model/data/'
RANDOM_HOSTNAME = '/' + '$(cat /etc/hostname)'
MODEL_DIR = '/workspace/minio/tensorboard/project/' + PROJECT_NAME + '/model/' + NAME + '/'
MODEL_DIR_WITH_HOST = MODEL_DIR + RANDOM_HOSTNAME
LOG_DIR = '/project/' + PROJECT_NAME + '/model/' + NAME + '/'
MINIO_PATH = 'minio/tensorboard/project/' + PROJECT_NAME + '/model/' + NAME + '/'
IP_ADDRESS = '35.85.65.89'

# minio parameters
S3_ENDPOINT = 'minio-service.kubeflow.svc.cluster.local:9000'
AWS_ENDPOINT_URL = "http://" + S3_ENDPOINT
AWS_ACCESS_KEY_ID = "minio"
AWS_SECRET_ACCESS_KEY = "minio123"
AWS_REGION = "us-east-1"
MINIO_ADDR = 'http://' + IP_ADDRESS + ':30238/'

# katib parameters
MAX_TRIAL_COUNT = 8
MAX_FAILED_TRIAL_COUNT = 4
PARALLEL_TRIAL_COUNT = 1
EPOCHS = 10
LOSS_GOAL = 0.000001
MIN_LR = 0.001
MAX_LR = 0.07
MIN_MOMENTUM = 0.5
MAX_MOMENTUM = 0.9
PYTORCH_FAHION_MNIST_IMAGE = 'kosehy/katib_pytorchjob:latest'

# auth info
USERNAME = "admin@kubeflow.org"
PASSWORD = "12341234"
NAMESPACE = 'admin'
HOST = 'http://' + IP_ADDRESS + ':31380'