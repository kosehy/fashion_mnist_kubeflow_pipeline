# how to setup gitlab-runner for kubernetes
https://gitlab.com/gitlab-org/charts/gitlab-runner

## git clone gitlab-runner
```
git clone git@gitlab.com:gitlab-org/charts/gitlab-runner.git
```
## go to gitlab-runner folder
```
cd gitlab-runner
```
## create docker-registry secret
```
mypassword='PASSWORD'
kubectl create secret docker-registry registry-docker \
--namespace gitlab \
--docker-username kosehy \
--docker-password $mypassword \
--docker-server https://index.docker.io/v1/  \
--docker-email kosehy@gmail.com

unset mypassword
```
## configure values.yaml
set gitlabUrl

```
gitlabUrl: https://gitlab.com/
```

copy registraion token from gitlab repo to values.yaml

set imagePullSecrets' name as registry-docker for docker login information

set config section as below
```
config: |
    [[runners]]
      builds_dir = "/workspace/"
      [runners.kubernetes]
        namespace = "{{.Release.Namespace}}"
        image = "ubuntu:20.04"
      [[runners.kubernetes.volumes.empty_dir]]
        name = "docker-certs"
        mount_path = "/certs/client"
        medium = "Memory"
```
set privileged value as true

## add variables to gitlab CI/CD variable tabs
add docker hub info
```
CI_REGISTRY
REGISTRY_USERNAME
REGISTRY_PASSWORD
```

## How to install gitlab-runner
```
helm package .
helm upgrade --install --namespace gitlab gitlab-runner --set rbac.create=true *.tgz
```

## How to uninstall gitlab-runner
```
helm uninstall --namespace gitlab gitlab-runner
```